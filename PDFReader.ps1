# add support for file select dialog
Add-Type -AssemblyName System.Windows.Forms

# read config file
$config = Get-Content -Path ".\config.ini"
$dll_path = $config | Select-String -Pattern "^dll_path=" | ForEach-Object { $_.ToString().Split("=")[1] }
$lastFolder = $config | Select-String -Pattern "^active_path=" | ForEach-Object { $_.ToString().Split("=")[1] }

# if we can't find the DLL via config.ini
if(!(Test-Path $dll_path))
{
    $openFileDialog = New-Object System.Windows.Forms.OpenFileDialog;
    $openFileDialog.Filter = "DLL file (*.dll)|*.dll|All files(*.*)|*.*";
    $openFileDialog.Title = "Select DLL File";

    if($openFileDialog.ShowDialog() -eq "OK")
    {
        $dll_path = $openFileDialog.FileName
    }
}
if(Test-Path $dll_path)
{
    [System.Reflection.Assembly]::LoadFrom($dll_path);
}


# set project name
$defaultProject = 'HW5-Kelso-Raglan'

$project = Read-Host -Prompt "Please enter project name or press Enter for default ('$defaultProject')"
if($project -eq "")
{
    $project = $defaultProject
}

# special requirements:
# Form-220 has items or materials to add to name
# Form-305 has people inducted to add to name
# MPDs require parsing "Monthly" as the form before changing name

# keys
$date_key = 'Conducted on'
$doc_key = 'Document No'
$name_key = 'Inductee Name' # for Form-305
$material_key = 'Item / Material Type' # form Form-220

# open UI to select folder for processing
$folderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog

if($lastFolder)
{
    $folderBrowser.SelectedPath = $lastFolder;
}

if($folderBrowser.ShowDialog() -eq "OK")
{
    $selectedFolder = $folderBrowser.SelectedPath;

    $config | ForEach-Object { if ($_ -match "^dll_path=") { "dll_path=$dll_path" } elseif ($_ -match "^active_path=") { "active_path=$selectedFolder"} else { $_ }} | Set-Content -Path ".\config.ini"

    $directory = Get-ChildItem -Path $selectedFolder -Filter *.pdf;

    foreach ($pdf in $directory) 
    { 
	    Write-Host "Processing - " $pdf.Name; 
	    $PdfReader = New-Object iTextSharp.text.pdf.PdfReader($pdf.FullName); 
    	    $strategy = New-Object 'iTextSharp.text.pdf.parser.SimpleTextExtractionStrategy';
	    $pagetext =[iTextSharp.text.pdf.parser.PdfTextExtractor]::GetTextFromPage($PdfReader,1, $strategy); 
	    $PdfReader.Close();
	    $form_extract = $pagetext.split("`n")[0];
	    $form = $form_extract.split(" ")[0];
        Remove-Variable -name form_extract;

        # custom naming conventions start here
	    if($form -Match '220')
	    { 
            $startMatIndex = $pagetext.IndexOf("Item / Material Type") + 20;
            $endMatIndex = $pagetext.IndexOf("Stockpile reference");
            $matString = $pagetext.Substring($startMatIndex, $endMatIndex - $startMatIndex);
    	    $matString = $matString.Replace('/', '-');
            $matString = $matString -replace '\r*\n', '';
            Write-Host $matString;
	    }
	    elseif($form -Match '305')
	    { 
		    $name_extract = $pagetext.split("`n") | Select-String $name_key;
		    $docName = "$name_extract".subString(14);
            Remove-Variable -name name_extract;
	    }
        elseif($form -Match 'Monthly')
        {
            $form = "MPD";
        }
        elseif($form -Match 'Permit')
        {
            $form = "PTE";
        }
        # TODO: handling ITPs

	    $date_extract = $pagetext.split("`n") | Select-String $date_key;
	    $doc_extract = $pagetext.split("`n") | Select-String $doc_key;
	    $docNum = "$doc_extract".subString(13);
        $docNum = $docNum.Replace('/', '-'); # quick fix for people entering poorly formatted dates into document number...
        Remove-Variable -name doc_extract;


	    $dateString = [datetime]::ParseExact("$date_extract".subString(13), 'dd.mm.yyyy', $null).ToString('yyyymmdd');
        Remove-Variable -name date_extract;


	    if($form -Match '305')
	    {
		    $updatedName = $form + '_' + $project+ '_' + $dateString + '_' + $docNum + ' (' + $docName + ').pdf';
            Remove-Variable -name docName;
	    }
	    elseif($form -Match '220')
	    {
		    $updatedName = $form + '_' + $project+ '_' + $dateString + '_' + $docNum + ' (' + $matString + ').pdf'; # 220
            Remove-Variable -name matString;
	    }
	    else
	    {
		    $updatedName = $form + '_' + $project+ '_' + $dateString + '_' + $docNum + '.pdf';
	    }

        $newPDFPath = [System.IO.Path]::Combine($selectedFolder, $updatedName)

	    Rename-Item -path $pdf.FullName -newName $newPDFPath;

        Remove-Variable -name docNum;
        Remove-Variable -name updatedName;
        Remove-Variable -name form;
        Remove-Variable -name dateString;
    }    
    
}
