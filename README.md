# iAuditor Processor


## Description
This processor is a PowerShell script that allows for batch renaming of PDF files downloaded from the iAuditor/SafetyCulture platform.

The script is currently set up based on the templates used by Transport for NSW for their Quality Assurance work.


## Installation
Simply copy the files to a folder. A brief explanation of the files follows:
* _itextsharp.dll_ is the primary iTextSharp library for reading PDF files.
* _BouncyCastle.crypto.dll_ is a cryptographic library that is required by iTextSharp.
* _config.ini_ is a text-formatted file used for storing variables that we will be using, such as location of DLLs and working folders (for convenience of repeat access).
* _PDFReader.ps1_ is the main script to be run.

## Usage
For systems not locked down and prevented from running scripts, it should be enough to run the PDFReader script itself.

For systems that are locked down and unable to directly run scripts (including TfNSW systems), it is recommended to open the ps1 file in either WordPad or PowerShell ISE and copy the full contents into a PowerShell terminal.

The location of the DLL file should be set in _config.ini_; this will need to be changed based on where it is located for your usage,and can be set while running the script.

If the script is being used repeatedly on the one project, the $defaultProject variable on _line 9_ should be modified accordingly:

`$defaultProject = 'HWx-Project-Name'`

In the future this may be migrated to the new config.ini file.

## Support
Issues should be added to the Issue Tracker. Alternatively I can be emailed for issues relating to work on construction projects.

## Roadmap
No major changes are planned at this stage. Features may be added on request.

## Contributing
There is likely to be little to no point contributing to this project due to its specialty.

## License
To be finalised.
